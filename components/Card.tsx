import React from 'react';


/**
 *  number of shapes [1,2,3]
 *  color of shapes [red, purple, green]
 *  fill of shapes [empty, fill, shaded]
 *  type of shape [diamond, oblong, squiggle]
 *
 */

const shapes = {
    diamond: <path d="M61.0472 0L120 20.3491L58.9528 40L0 19.6509L61.0472 0Z" />,
    oblong: <rect width="120" height="40" rx="20" />,
    squiggle: <path d="M8.0535 4.84511C15.5554 -1.31034 23.7437 1.05639 34.0268 1.05639C44.3098 1.05639 49.8568 3.36553 60 4.84511C70.1432 6.3247 75.6902 8.63384 85.9732 8.63384C96.2563 8.63384 103.145 -3.59764 111.946 1.05639C123.522 7.17734 121.813 27.0593 111.946 35.1549C104.445 41.3103 96.2563 38.9436 85.9732 38.9436C75.6902 38.9436 70.1432 36.6345 60 35.1549C49.8568 33.6753 44.3098 31.3662 34.0268 31.3662C23.7437 31.3662 16.855 43.5976 8.0535 38.9436C-3.52211 32.8227 -1.81294 12.9407 8.0535 4.84511Z" />;
}
 
const Card = (props) => {

    const shape = shapes[props.shape];
    const bg = props.fill === 'empty' ? '#f8f8f8' : props.color;
    const shaded = props.fill === 'shaded';
    const border = props.color;

    const patternId = `${props.shape}${props.fill}${props.number}${props.color}`;

    const handleClick = () => {
        props.onClick({
            shape: props.shape,
            color: props.color,
            fill: props.fill,
            number: props.number
        })
    }

    return (
        <div className={`card bg-white transition cursor-pointer p-6 rounded-md shadow-xl max-w-min border-2 mb-6 mx-auto ${props.selected ? 'border-blue-500 transform -translate-y-2' : 'border-transparent'}`} onClick={handleClick}>
            <div className="flex items-center">
                <svg width="80" height="106" viewBox="-2 -2 122 162" fill={bg} stroke={border} strokeWidth="2" xmlns="http://www.w3.org/2000/svg">
                    {
                        shaded ?
                            <pattern id={patternId} patternUnits="userSpaceOnUse" width="4" height="4">
                                <path d="M-1,1 l2,-2 M0,4 l4,-4 M3,5 l2,-2" style={{ stroke: bg, strokeWidth: "1" }} /> 
                            </pattern>
                            :
                            null
                    }

                    {
                        props.number == 1 ?
                            <g transform="translate(0, 60)" fill={shaded ? `url(#${patternId})` : null}>
                                {shape}
                            </g>
                            :
                            null
                    }
                    {
                        props.number == 2 ?
                            <>
                                <g transform="translate(0, 30)" fill={shaded ? `url(#${patternId})` : null}>
                                    {shape}
                                </g>
                                <g transform="translate(0, 90)" fill={shaded ? `url(#${patternId})` : null}>
                                    {shape}
                                </g>
                            </>
                            :
                            null
                    }

                    {
                        props.number == 3 ?
                            <>
                                <g fill={shaded ? `url(#${patternId})` : null}>
                                    {shape}
                                </g>
                                <g transform="translate(0, 50)" fill={shaded ? `url(#${patternId})` : null}>
                                    {shape}
                                </g>
                                <g transform="translate(0, 100)" fill={shaded ? `url(#${patternId})` : null}>
                                    {shape}
                                </g>
                            </>
                            :
                            null
                    }

                </svg>
            </div>
        </div>
    )
}
// Card.defaultProps = {
//     number: 3,
//     shape: 'diamond',
//     color: 'red',
//     fill: 'empty'
// }
export default Card;

