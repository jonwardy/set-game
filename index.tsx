import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import Card from './components/Card';
import { fromEvent, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

const counts = [1, 2, 3];
const shapes = ['diamond', 'oblong', 'squiggle'];
const colors = ['green', 'purple', 'red'];
const fills = ['shaded', 'empty', 'filled'];

const allcardsArray = [];
// need to represent attribs as numbers so we can do some maths on it later
counts.forEach(
    (count) => shapes.forEach(
        (shape, shapeIdx) => colors.forEach(
            (color, colorIdx) => fills.forEach(
                (fill, fillIdx) =>
                    allcardsArray.push({
                        number: count,
                        shape: shapeIdx + 1,
                        color: colorIdx + 1,
                        fill: fillIdx + 1
                    })
            )
        )
    )
)

const allcards = allcardsArray.reduce((acc, cur) => {
    const id = Object.values(cur).join('');
    acc[id] = cur;
    return acc;
}, {})

console.log(allcards, allcardsArray)


const random = [...allcardsArray].sort(() => Math.random() < 0.5 ? 1 : -1).slice(0, 12);


const App = () => {

    const [cards, setCards] = useState(random);
    const [selected, setSelected] = useState({});

    const handleReset = () => {
        setCards([...allcardsArray].sort(() => Math.random() < 0.5 ? 1 : -1).slice(0, 12));
        setSelected({});
    }

    const event$$ = new Subject();

    const handleCardClick = (id) => {
        event$$.next(id);
    }

    event$$.pipe(
        tap(id => {
            const newSelection = {...selected};
            console.log(typeof id, newSelection)
            if (newSelection[id]) {
                delete newSelection[id];
            } else {
                newSelection[id] = true;
            }
            setSelected(newSelection);
        })
    ).subscribe((id) => { console.log(id, selected) });

    return (
        <>
            {
                cards.map((item) => {
                    const cardId = Object.values(item).join('');
                    return (
                        <Card
                            number={item.number}
                            shape={shapes[item.shape - 1]}
                            fill={fills[item.fill - 1]}
                            color={colors[item.color - 1]}
                            key={cardId}
                            onClick={() => { handleCardClick(cardId) }}
                            selected={selected[cardId]}
                        />
                    )
                }
                )
            }
            <button onClick={handleReset} className="p-3 bg-gray-300">Reset</button>
        </>
    )
}

var mount = document.getElementById('root');
ReactDOM.render(<App />, mount);